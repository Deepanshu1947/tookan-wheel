package com.example.deepanshu.TokanWheel.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.deepanshu.TokanWheel.GoogleMapsApi.GeoCodingLocation;
import com.example.deepanshu.TokanWheel.GoogleMapsApi.GooglePlacesAutoCompleteAdapter;
import com.example.deepanshu.TokanWheel.R;
import com.example.deepanshu.TokanWheel.Utilities.Conventions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class GoogleMapLocationCallActivity extends FragmentActivity implements GoogleMap.OnMyLocationChangeListener, OnMapReadyCallback, Conventions, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, AdapterView.OnItemClickListener {
    GoogleMap googleMap;
    private AutoCompleteTextView autoCompleteTextView;
    private GooglePlacesAutoCompleteAdapter autoCompleteAdapter;
    ImageButton ivBack;
    TextView tvActivityStatus;
    Button btDone;
    boolean locationSelected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_map);
        init();
    }

    public void init() {
        ivBack = (ImageButton) findViewById(R.id.button_back);
        tvActivityStatus = (TextView) findViewById(R.id.status_text);
        tvActivityStatus = (TextView) findViewById(R.id.status_text);
        btDone = (Button) findViewById(R.id.done_google_map);
        tvActivityStatus.setText(R.string.title_activity_google_map);
        ivBack.setOnClickListener(GoogleMapLocationCallActivity.this);
        btDone.setOnClickListener(this);
        //for getting the map

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);
        googleMap = mapFragment.getMap();

        //Code to check Internet

        int status = isGooglePlayServicesAvailable();
        if (status != ConnectionResult.SUCCESS) {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        } else {
            //for my location button visibility

            googleMap.setMyLocationEnabled(true);
            googleMap.setOnMyLocationChangeListener(this);

            //to change location button image resource

            ImageView locationButton = (ImageView) ((View) mapFragment.getView().findViewById(1).getParent()).findViewById(2);
            locationButton.setImageResource(R.drawable.selector_current_location);
        }

        //setting AutoCompleteTextView userDefine function

        setAutoCompleteTextView();

    }

    @Override
    public void onBackPressed() {

    }

    public void setAutoCompleteTextView() {
        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);
        autoCompleteTextView.setOnItemClickListener(this);
        autoCompleteAdapter = new GooglePlacesAutoCompleteAdapter(this, android.R.layout.simple_list_item_1);
        autoCompleteTextView.setAdapter(autoCompleteAdapter);
    }

    @Override
    public void onMapReady(GoogleMap map) {
    }

    @Override
    public void onMyLocationChange(Location location) {
        if (!locationSelected) {
            setLastLocation(location.getLatitude(), location.getLongitude());
        }
    }

    public void setLastLocation(Double latitude, Double longitude) {

        //setting the location
        LatLng latLng = new LatLng(latitude, longitude);
        //setting the marker
        googleMap.clear();
        googleMap.addMarker(new MarkerOptions().position(latLng).anchor(0.5f, 0.5f).icon(BitmapDescriptorFactory.fromResource(R.mipmap.your_location_icon)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

    }


    private int isGooglePlayServicesAvailable() {
        //checking the google services(Internet Connection)
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        return status;

    }


//      Called when the Activity could not connect to Google Play services and the auto manager could resolve the error automatically.

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Log.e(TAG, "onConnectionFailed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();

        switch (v.getId()) {
            case R.id.button_back:
                intent.putExtra("address", autoCompleteTextView.getText().toString());
                setResult(RESULT_CANCELED, intent);
                GoogleMapLocationCallActivity.this.finish();
                break;
            case R.id.done_google_map:

                intent.putExtra("address", autoCompleteTextView.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
        locationSelected = true;
        hideKeybord();
        String agentAddress = (String) autoCompleteAdapter.getItem(position);
        int status = isGooglePlayServicesAvailable();
        if (status != ConnectionResult.SUCCESS) {
            Toast.makeText(GoogleMapLocationCallActivity.this, MSG_NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();
        } else {
            GeoCodingLocation convertLocationToLatLng = new GeoCodingLocation();
            convertLocationToLatLng.getLatLongFromAddress(agentAddress, getApplicationContext(), new LatLngHandler());
        }
//        Toast.makeText(this, agentAddress, Toast.LENGTH_SHORT).show();

    }

    private class LatLngHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            double[] latLong = bundle.getDoubleArray(LAT_LONG_BUNDLE);

            switch (msg.what) {
                case ADDRESS_FOUND_MESSAGE:
                    locationSelected = true;
                    break;
                case ADDRESS_NOT_FOUND_MESSAGE:
                default:
                    latLong[0] = DEFAULT_LATITUDE;
                    latLong[1] = DEFAULT_LONGITUDE;
                    Toast.makeText(GoogleMapLocationCallActivity.this, MSG_ADDRESS_NOT_FOUND, Toast.LENGTH_SHORT).show();
            }
            setLastLocation(latLong[0], latLong[1]);
        }
    }

    public void hideKeybord() {
        //to hide keyboard if shown
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive())
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

}
