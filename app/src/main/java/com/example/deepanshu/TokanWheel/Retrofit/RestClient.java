package com.example.deepanshu.TokanWheel.Retrofit;

import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by Deepanshu on 12/18/2015.
 */
public class RestClient {
    ApiService service;
    final String BASE_URL = "http://52.27.143.179:8001";

    public RestClient() {
        OkHttpClient okHttpClient = new OkHttpClient();
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setClient(new OkClient(okHttpClient))
                .setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        service = restAdapter.create(ApiService.class);

    }

    public ApiService getService() {
        return service;
    }
}
