package com.example.deepanshu.TokanWheel.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.deepanshu.TokanWheel.Entities.ApiUserResponse;
import com.example.deepanshu.TokanWheel.R;
import com.example.deepanshu.TokanWheel.Retrofit.RestClient;
import com.example.deepanshu.TokanWheel.Utilities.Validations;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SignUpActivity extends BaseActivity {
    EditText etFirstName, etLastName, etPassword, etConfirmPassword, etEmail, etMobileNumber;
    TextView tvSignIn, tvTerms;
    Button btSignUp;
    Validations validate;
    String fName, lName, emailId, mobNo, password, confirmPassword;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        init();
        setOnClick();

    }

    public void init() {
        etFirstName = (EditText) findViewById(R.id.et_first_name);
        etLastName = (EditText) findViewById(R.id.et_last_name);
        etPassword = (EditText) findViewById(R.id.et_password);
        etConfirmPassword = (EditText) findViewById(R.id.et_confirm_password);
        etEmail = (EditText) findViewById(R.id.et_email);
        etMobileNumber = (EditText) findViewById(R.id.et_mobile_number);
        tvSignIn = (TextView) findViewById(R.id.tv_sign_in);
        tvTerms = (TextView) findViewById(R.id.tv_terms);
        btSignUp = (Button) findViewById(R.id.button_sign_up);
        validate = new Validations();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);


    }

    public void setOnClick() {
        setActivityHeader(R.string.title_activity_sign_up);
        ivBack.setOnClickListener(this);
        tvSignIn.setText(Html.fromHtml(getString(R.string.already_member)));
        tvTerms.setText(Html.fromHtml(getString(R.string.agree_terms)));
        tvSignIn.setOnClickListener(SignUpActivity.this);
        tvTerms.setOnClickListener(SignUpActivity.this);
        btSignUp.setOnClickListener(SignUpActivity.this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_terms:
                startActivity(new Intent(SignUpActivity.this, TermsConditionsActivity.class));
                break;
            case R.id.button_sign_up:
                validate();
                break;
            case R.id.tv_sign_in:
            case R.id.button_back:
                startActivity(new Intent(SignUpActivity.this, SignInActivity.class));
                finish();
                break;
        }
    }

    public void validate() {

        fName = etFirstName.getText().toString();
        lName = etLastName.getText().toString();
        emailId = etEmail.getText().toString();
        mobNo = etMobileNumber.getText().toString();
        password = etPassword.getText().toString();
        confirmPassword = etConfirmPassword.getText().toString();

        Boolean validFirstName = validate.nameValidator(fName);
        Boolean validLastName = validate.nameValidator(lName);
        Boolean validEmail = validate.emailValidator(emailId);
        Boolean validPhone = validate.phoneValidator(mobNo);
        Boolean validPassword = validate.passwordValidator(password);
         Boolean validConfirmPassword = validate.confirmPasswordValidator(password, confirmPassword);

        if (validFirstName && validLastName && validEmail && validPassword && validConfirmPassword ) {
            retrofitCall();
        } else if (!validFirstName)
            showToast(INVALID_FIRST_NAME);
        else if (!validLastName)
            showToast(INVALID_LAST_NAME);
        else if (!validEmail)
            showToast(INVALID_EMAIL);
        else if (!validPassword)
            showToast(INVALID_PASSWORD);
        else if (!validConfirmPassword)
            showToast(INVALID_CONFIRM_PASSWORD);
        else if (!validPhone)
            showToast(INVALID_PHONE);

    }

    public void retrofitCall() {

        hideKeybord();
        showProgressDialog(PROCESSING);
        new RestClient().getService().getRegister(
                emailId, mobNo,DEVICE_TYPE,
                sharedPreferences.getString(DEVICE_TOKEN, ""),
               new Callback<ApiUserResponse>() {
                    @Override
                    public void success(ApiUserResponse registerResponse, Response response) {
                        sharedPreferences.edit().putString(ACCESS_TOKEN, registerResponse.getData().getToken());
                        showToast(SUCCESS);
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(ACCESS_TOKEN, registerResponse.getData().getToken().toString());
                        editor.commit();
                        startActivity(new Intent(SignUpActivity.this, FragmentsMainActivity.class));
                        finish();
                        hideProgressDialog();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        hideProgressDialog();
                        showToast(error.getMessage());
                    }
                });
    }
}
