package com.example.deepanshu.TokanWheel.Entities;

/**
 * Created by DEEPANSHU on 19-12-2015.
 */
public class TaskData {
    String taskStatus, taskId;
    TookanStatusClass[] tookanStatusAfterCreateTask;
    PickUpTask[] taskDetails;

    public PickUpTask[] getTaskDetails() {
        return taskDetails;
    }

    public void setTaskDetails(PickUpTask[] taskDetails) {
        this.taskDetails = taskDetails;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public TookanStatusClass[] getTookanStatusAfterCreateTask() {
        return tookanStatusAfterCreateTask;
    }

    public void setTookanStatusAfterCreateTask(TookanStatusClass[] tookanStatusAfterCreateTask) {
        this.tookanStatusAfterCreateTask = tookanStatusAfterCreateTask;
    }










    public class TookanStatusClass {
        int job_id, tookanPickUpJobId, tookanDropOffJobId;
        String tookanPickUpJobStatus, tookanDropOffJobStatus,
                tookanDeliveryHash, tookanPickUpHash, order_id;

        public int getJob_id() {
            return job_id;
        }

        public void setJob_id(int job_id) {
            this.job_id = job_id;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getTookanDeliveryHash() {
            return tookanDeliveryHash;
        }

        public void setTookanDeliveryHash(String tookanDeliveryHash) {
            this.tookanDeliveryHash = tookanDeliveryHash;
        }

        public int getTookanDropOffJobId() {
            return tookanDropOffJobId;
        }

        public void setTookanDropOffJobId(int tookanDropOffJobId) {
            this.tookanDropOffJobId = tookanDropOffJobId;
        }

        public String getTookanDropOffJobStatus() {
            return tookanDropOffJobStatus;
        }

        public void setTookanDropOffJobStatus(String tookanDropOffJobStatus) {
            this.tookanDropOffJobStatus = tookanDropOffJobStatus;
        }

        public String getTookanPickUpHash() {
            return tookanPickUpHash;
        }

        public void setTookanPickUpHash(String tookanPickUpHash) {
            this.tookanPickUpHash = tookanPickUpHash;
        }

        public int getTookanPickUpJobId() {
            return tookanPickUpJobId;
        }

        public void setTookanPickUpJobId(int tookanPickUpJobId) {
            this.tookanPickUpJobId = tookanPickUpJobId;
        }

        public String getTookanPickUpJobStatus() {
            return tookanPickUpJobStatus;
        }

        public void setTookanPickUpJobStatus(String tookanPickUpJobStatus) {
            this.tookanPickUpJobStatus = tookanPickUpJobStatus;
        }
    }
}
