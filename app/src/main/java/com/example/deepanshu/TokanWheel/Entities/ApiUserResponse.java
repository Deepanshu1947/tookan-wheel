package com.example.deepanshu.TokanWheel.Entities;

/**
 * Created by Deepanshu on 12/18/2015.
 */
public class ApiUserResponse {
    String statusCode, message;
    LoginValues data;

    public LoginValues getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public class LoginValues {
        public String getToken() {
            return accessToken;
        }

        public void setToken(String token) {
            this.accessToken = token;
        }

        String accessToken;

    }


}
