package com.example.deepanshu.TokanWheel.Fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.example.deepanshu.TokanWheel.Activities.FragmentsMainActivity;
import com.example.deepanshu.TokanWheel.Adapters.ExpendableListAdapter;
import com.example.deepanshu.TokanWheel.R;

import java.text.SimpleDateFormat;

public class HomeFragment extends BaseFragment {
    Button btCompleted, btPending, btOnGoing;
    ExpendableListAdapter listAdapter;
    public static int flag = 1;//past onGoing UpComing
    ExpandableListView expList;
    Activity currActivity;
    View myView;
    SimpleDateFormat sdf = new SimpleDateFormat("h:mm a, MMMM dd,yyyy");
    //variable for displaying the invitation time left (a text view visibility through adapter)

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.fragment_tasks, container, false);
        return myView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        setOnClick();
    }


    public void init() {

        currActivity = getActivity();
        //initializing the xml widgets
        btCompleted = (Button) myView.findViewById(R.id.button_scheduled);
        btPending = (Button) myView.findViewById(R.id.button_pending);
        btOnGoing = (Button) myView.findViewById(R.id.button_on_going);
        expList = (ExpandableListView) myView.findViewById(R.id.appointments_list);

        //selecting the butons from two
        setButoonSelected(btOnGoing, btCompleted, btPending);

        //creating temporary data
        listAdapter = new ExpendableListAdapter(currActivity, FragmentsMainActivity.initResponse);

    }


    public void setOnClick() {
        //for setting the
        btCompleted.setOnClickListener(HomeFragment.this);
        btPending.setOnClickListener(HomeFragment.this);
        btOnGoing.setOnClickListener(HomeFragment.this);
        expList.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_scheduled:
                flag=0;
                listAdapter.notifyDataSetChanged();
                setButoonSelected(btCompleted, btPending, btOnGoing);
                break;
            case R.id.button_pending:

                flag=2;
                listAdapter.notifyDataSetChanged();
                setButoonSelected(btPending, btCompleted, btOnGoing);
                break;
            case R.id.button_on_going:
                flag=1;

                listAdapter.notifyDataSetChanged();
                setButoonSelected(btOnGoing, btPending, btCompleted);
                break;
        }
    }

    // for setting out the button background of selected button and second one non selected and its drawable
    public void setButoonSelected(Button btSelected, Button btNotSelected1, Button btNotSelected2) {
        btSelected.setBackgroundResource(R.mipmap.big_btn_pressed);
        btSelected.setTextColor(Color.parseColor("#ffffff"));
        btNotSelected1.setBackgroundResource(R.drawable.big_button_selector);
        btNotSelected1.setTextColor(Color.parseColor("#474747"));
        btNotSelected2.setBackgroundResource(R.drawable.big_button_selector);
        btNotSelected2.setTextColor(Color.parseColor("#474747"));
    }
}
