package com.example.deepanshu.TokanWheel.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.deepanshu.TokanWheel.Entities.ApiUserResponse;
import com.example.deepanshu.TokanWheel.Entities.PickUpTask;
import com.example.deepanshu.TokanWheel.R;
import com.example.deepanshu.TokanWheel.Retrofit.RestClient;
import com.example.deepanshu.TokanWheel.Utilities.Validations;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AddItemActivity extends BaseActivity {
    EditText eTItemName, eTItemDescription;
    Button bNext, btDone;
    Validations validations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        init();
        setOnClick();
    }

    @Override
    public void init() {
        validations = new Validations();
        eTItemName = (EditText) findViewById(R.id.edit_text_item_name);
        eTItemDescription = (EditText) findViewById(R.id.edit_text_item_description);
        bNext = (Button) findViewById(R.id.button_next);
        btDone = (Button) findViewById(R.id.button_done);
        if (pickUpTaskList.size() > 0) {
            PickUpTask pickUpObj = pickUpTaskList.get(pickUpTaskList.size() - 1);
            eTItemName.setText(pickUpObj.getItemName());
            eTItemDescription.setText(pickUpObj.getDescription());
        }
    }

    @Override
    public void setOnClick() {
        bNext.setOnClickListener(this);
        btDone.setOnClickListener(AddItemActivity.this);

        if (pickUpTaskList.size() == 0) {
            btDone.setVisibility(View.GONE);
        }
        setActivityHeader(R.string.title_activity_add_item);
        setIvBackOnClick();
    }

    @Override
    public void onBackPressed() {
        if (pickUpTaskList.size() == 0) {
            Intent intent = new Intent(AddItemActivity.this, FragmentsMainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_next:
                validate();
                break;

            case R.id.button_done:
                try {
                    callRest();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public void callRest() throws JSONException {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(AddItemActivity.this);
        String token = sharedPreferences.getString(ACCESS_TOKEN, "");
        hideKeybord();
        showProgressDialog(PROCESSING);
        Gson gson = new GsonBuilder().create();

//        JsonArray jsonArray = gson.toJsonTree(pickUpTaskList).getAsJsonArray();
        JSONArray jsonArray = new JSONArray(new Gson().toJson(pickUpTaskList));//gson.toJsonTree(pickUpTaskList).getAsJsonArray();
        Log.i("Array", pickUpTaskList.toString());

        new RestClient().getService().postAddress(BEARER + token, jsonArray
                , new Callback<ApiUserResponse>() {
            @Override
            public void success(ApiUserResponse apiUserResponse, Response response) {
                pickUpTaskList.clear();

                Intent intent = new Intent(AddItemActivity.this, FragmentsMainActivity.class);
                startActivity(intent);
                AddItemActivity.this.finish();
            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressDialog();
                Log.i("Retro error", "" + error.toString());
                showToast(error.getMessage());
            }
        });
    }

    public void validate() {
        Boolean validItemName = validations.nameValidator(eTItemName.getText().toString());
        Boolean validItemDescription = (eTItemDescription.getText().toString().length() > 5);
        if ((validItemName == true) && (validItemDescription == true)) {
            Intent intent = new Intent(AddItemActivity.this, PickUpDeliveryActivity.class);
            intent.putExtra(ITEM_NAME, "" + eTItemName.getText().toString());
            intent.putExtra(TASK_DESC, "" + eTItemDescription.getText().toString());
            startActivity(intent);
            this.finish();
        } else if (validItemName == false) {
            showToast(INVALID_FIRST_NAME);
        } else if (validItemDescription == false) {
            showToast(INVALID_ABOUT_ME);
        }
    }
}