package com.example.deepanshu.TokanWheel.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.deepanshu.TokanWheel.Activities.ShowLocationOnMapActivity;
import com.example.deepanshu.TokanWheel.Entities.InitialResponse;
import com.example.deepanshu.TokanWheel.Entities.PickUpTask;
import com.example.deepanshu.TokanWheel.Entities.TaskData;
import com.example.deepanshu.TokanWheel.Fragments.HomeFragment;
import com.example.deepanshu.TokanWheel.R;

/**
 * Created by Deepanshu on 12/18/2015.
 */
public class ExpendableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    TaskData[] initialResponse;

    public ExpendableListAdapter(Context context, InitialResponse initialResponse) {
        this.context = context;
        this.initialResponse = initialResponse.getData();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        PickUpTask data = initialResponse[groupPosition].getTaskDetails()[childPosition];
        return data;
    }

    @Override
    public int getChildTypeCount() {
        return super.getChildTypeCount();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final PickUpTask child = (PickUpTask) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.view_item_expanded, null);
        }
        ImageView ivlocation = (ImageView) convertView.findViewById(R.id.button_location);

        TextView tvProduct = (TextView) convertView.findViewById(R.id.tv_package);
        TextView tvLocationStatus = (TextView) convertView.findViewById(R.id.tv_location_status);
        tvLocationStatus.setText(child.getPickUpAddress());
        if (HomeFragment.flag == 1) {//onGOING
            tvProduct.setText("onGoing " + child.getItemName().toString());

        } else if (HomeFragment.flag == 2)//upcoming
        {
            tvProduct.setText("upComing " + child.getItemName().toString());
        } else if (HomeFragment.flag == 0)//past
        {
            tvProduct.setText("Past " + child.getItemName().toString());

        }
        ivlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent showLocationPage = new Intent(context, ShowLocationOnMapActivity.class);
                if (HomeFragment.flag == 1) {//onGOING
                    showLocationPage.putExtra("location", new Double[]{child.getDeliveryLat(), child.getDeliveryLong()});
                    context.startActivity(showLocationPage);
                } else if (HomeFragment.flag == 2)//upcoming
                {

                    showLocationPage.putExtra("location", new Double[]{child.getPickUpLongitude(), child.getPickUpLongitude()});
                    context.startActivity(showLocationPage);
                } else if (HomeFragment.flag == 0)//past
                {
                }
            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (initialResponse == null || initialResponse[groupPosition].getTaskDetails() == null) {
            return 0;
        } else {
            return initialResponse[groupPosition].getTaskDetails().length;
        }

    }

    @Override
    public Object getGroup(int groupPosition) {

        return initialResponse[groupPosition];
    }

    @Override
    public int getGroupCount() {
        if (initialResponse == null) {
            return 0;
        } else {
            return initialResponse.length;
        }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        TaskData group = (TaskData) getGroup(groupPosition);
        LayoutInflater inf = LayoutInflater.from(context);
        convertView = inf.inflate(R.layout.view_item, parent, false);

        TextView tvTask = (TextView) convertView.findViewById(R.id.tv_task);
        TextView tvDate = (TextView) convertView.findViewById(R.id.tv_time_date);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.image_view_profile_pic);

        TextView tvName = (TextView) convertView.findViewById(R.id.profile_name);
        TextView tvProfilePhone = (TextView) convertView.findViewById(R.id.profile_phone);

        imageView.setImageResource(R.mipmap.user_icon);
        tvName.setText("Temp Name " + groupPosition);

        tvProfilePhone.setText("Temp Mobile " + groupPosition);

        tvTask.setText(group.getTaskId().toString());

//        tvDate.setText(group.getTookanStatusAfterCreateTask()[0].get.toString());

        return convertView;
    }
}
