package com.example.deepanshu.TokanWheel.Entities;

import java.io.Serializable;

/**
 * Created by Deepanshu on 12/18/2015.
 */
public class PickUpTask implements Serializable {
    String pickUpPhoneNo, pickUpName,
            pickUpEmail,
            pickUpAddress, pickUpDateTime,
            deliveryAddress, dropOffDateTime, deliveryPhoneNo, deliveryName, deliveryEmail,item,description;
    Double pickUpLatitude,
            pickUpLongitude, deliveryLat,
            deliveryLong;

    public PickUpTask(String deliveryAddress, String deliveryEmail, Double deliveryLat, Double deliveryLong, String deliveryName, String deliveryPhoneNo, String dropOffDateTime, String itemDescription, String item
            , String pickUpAddress, String pickUpDateTime, String pickUpEmail, Double pickUpLatitude, Double pickUpLongitude, String pickUpName, String pickUpPhoneNo) {
        this.deliveryAddress = deliveryAddress;
        this.deliveryEmail = deliveryEmail;
        this.deliveryLat = deliveryLat;
        this.deliveryLong = deliveryLong;
        this.deliveryName = deliveryName;
        this.deliveryPhoneNo = deliveryPhoneNo;
        this.dropOffDateTime = dropOffDateTime;
        this.description = itemDescription;
        this.item = item;
        this.pickUpAddress = pickUpAddress;
        this.pickUpDateTime = pickUpDateTime;
        this.pickUpEmail = pickUpEmail;
        this.pickUpLatitude = pickUpLatitude;
        this.pickUpLongitude = pickUpLongitude;
        this.pickUpName = pickUpName;
        this.pickUpPhoneNo = pickUpPhoneNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getItemName() {
        return item;
    }

    public void setItemName(String item) {
        this.item = item;
    }

    public PickUpTask() {
    }


    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDeliveryEmail() {
        return deliveryEmail;
    }

    public void setDeliveryEmail(String deliveryEmail) {
        this.deliveryEmail = deliveryEmail;
    }

    public Double getDeliveryLat() {
        return deliveryLat;
    }

    public void setDeliveryLat(Double deliveryLat) {
        this.deliveryLat = deliveryLat;
    }

    public Double getDeliveryLong() {
        return deliveryLong;
    }

    public void setDeliveryLong(Double deliveryLong) {
        this.deliveryLong = deliveryLong;
    }

    public String getDeliveryName() {
        return deliveryName;
    }

    public void setDeliveryName(String deliveryName) {
        this.deliveryName = deliveryName;
    }

    public String getDeliveryPhoneNo() {
        return deliveryPhoneNo;
    }

    public void setDeliveryPhoneNo(String deliveryPhoneNo) {
        this.deliveryPhoneNo = deliveryPhoneNo;
    }

    public String getDropOffDateTime() {
        return dropOffDateTime;
    }

    public void setDropOffDateTime(String dropOffDateTime) {
        this.dropOffDateTime = dropOffDateTime;
    }

    public String getPickUpAddress() {
        return pickUpAddress;
    }

    public void setPickUpAddress(String pickUpAddress) {
        this.pickUpAddress = pickUpAddress;
    }

    public String getPickUpDateTime() {
        return pickUpDateTime;
    }

    public void setPickUpDateTime(String pickUpDateTime) {
        this.pickUpDateTime = pickUpDateTime;
    }

    public String getPickUpEmail() {
        return pickUpEmail;
    }

    public void setPickUpEmail(String pickUpEmail) {
        this.pickUpEmail = pickUpEmail;
    }

    public Double getPickUpLatitude() {
        return pickUpLatitude;
    }

    public void setPickUpLatitude(Double pickUpLatitude) {
        this.pickUpLatitude = pickUpLatitude;
    }

    public Double getPickUpLongitude() {
        return pickUpLongitude;
    }

    public void setPickUpLongitude(Double pickUpLongitude) {
        this.pickUpLongitude = pickUpLongitude;
    }

    public String getPickUpName() {
        return pickUpName;
    }

    public void setPickUpName(String pickUpName) {
        this.pickUpName = pickUpName;
    }

    public String getPickUpPhoneNo() {
        return pickUpPhoneNo;
    }

    public void setPickUpPhoneNo(String pickUpPhoneNo) {
        this.pickUpPhoneNo = pickUpPhoneNo;
    }
}
