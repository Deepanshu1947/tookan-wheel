package com.example.deepanshu.TokanWheel.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.deepanshu.TokanWheel.Entities.AddressDetails;
import com.example.deepanshu.TokanWheel.Entities.AddressRetrieve;
import com.example.deepanshu.TokanWheel.Entities.ApiUserResponse;
import com.example.deepanshu.TokanWheel.Entities.PickUpTask;
import com.example.deepanshu.TokanWheel.Entities.SaveAddressResponse;
import com.example.deepanshu.TokanWheel.GoogleMapsApi.GeoCodingLocation;
import com.example.deepanshu.TokanWheel.R;
import com.example.deepanshu.TokanWheel.Retrofit.RestClient;
import com.example.deepanshu.TokanWheel.Utilities.Validations;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PickUpDeliveryActivity extends BaseActivity {
    EditText etZip, etCity, etStreet, etState, etSuite, etName, etMobile, etEmail;
    Button btNext, btSaveAddress;
    RelativeLayout rlSearchMap;
    Validations validate;
    Intent intent;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String name, mobile, email;
    Boolean screenView = true;
    Intent currIntent;
    PickUpTask pickUpObj = new PickUpTask();
    final ArrayList<AddressDetails> favAddress = new ArrayList();
    android.support.v4.widget.DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mDrawerToggle;
    ArrayAdapter drawerAdapter;
    LinearLayout drawerLayout;
    ListView drawerList;
    AddressDetails currAddress = new AddressDetails();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_up_delivery);
        init();
        setOnClick();
    }

    public void init() {
        currIntent = getIntent();
        pickUpObj.setItemName(currIntent.getStringExtra(ITEM_NAME));
        pickUpObj.setDescription(currIntent.getStringExtra(TASK_DESC));
        btNext = (Button) findViewById(R.id.button_add_another);
        btSaveAddress = (Button) findViewById(R.id.button_save_address);
        etZip = (EditText) findViewById(R.id.et_zip);
        etName = (EditText) findViewById(R.id.et_name);
        etMobile = (EditText) findViewById(R.id.et_mobile);
        etCity = (EditText) findViewById(R.id.et_city);
        etState = (EditText) findViewById(R.id.et_state);
        etStreet = (EditText) findViewById(R.id.et_street);
        etSuite = (EditText) findViewById(R.id.et_apartment);
        etEmail = (EditText) findViewById(R.id.et_mail);
        rlSearchMap = (RelativeLayout) findViewById(R.id.rl_search_map);
        validate = new Validations();
        setNavigationDrawer();
        getFavAddress();
    }

    public void setOnClick() {
        btNext.setOnClickListener(PickUpDeliveryActivity.this);
        btSaveAddress.setOnClickListener(PickUpDeliveryActivity.this);
        rlSearchMap.setOnClickListener(PickUpDeliveryActivity.this);

        setActivityHeader(R.string.title_activity_pick_up);
        btNext.setText("Next");

        ivBack.setImageResource(R.drawable.menu_bar_selector);
        ivBack.setOnClickListener(PickUpDeliveryActivity.this);
    }

    @Override
    public void setIvBackOnClick() {

    }

    @Override
    public void onBackPressed() {

    }

    public void setNavigationDrawer() {

        //initializing the layout variables
        mDrawerLayout = (android.support.v4.widget.DrawerLayout) findViewById(R.id.drawer_layout_base_address);
        drawerLayout = (LinearLayout) findViewById(R.id.ll_navigation_drawer);
        drawerList = (ListView) findViewById(R.id.lv_fav_address);

        //Setting the name and pic

        //Setting the conent of nav drawer list view
        drawerAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, favAddress) {
            @Override
            public int getCount() {
                if (favAddress == null)
                    return 0;
                else
                    return favAddress.size();
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inf = LayoutInflater.from(PickUpDeliveryActivity.this);
                convertView = inf.inflate(R.layout.fav_location_card, parent, false);
                TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
                String address = new String();
                if (favAddress.get(position).getSuite() != null)
                    address = address + " " + favAddress.get(position).getSuite();
                if (favAddress.get(position).getStreet() != null)
                    address = address + " " + favAddress.get(position).getStreet();

                tvName.setText(address);
                return convertView;
            }
        };
        drawerList.setAdapter(drawerAdapter);

        //to perform actions on navigation drawer list menu
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDrawerLayout.closeDrawer(drawerLayout);
                if (favAddress != null) {
                    currAddress = favAddress.get(position);
                    etStreet.setText(favAddress.get(position).getStreet());
                    etZip.setText(favAddress.get(position).getZip());
                    etState.setText(favAddress.get(position).getState());
                    etCity.setText(favAddress.get(position).getCity());
                    etSuite.setText(favAddress.get(position).getSuite());
                } else
                    showToast("Fail to Import");
            }
        });

        // to open and close navigation drawer on swipe or click
        mDrawerToggle = new ActionBarDrawerToggle(this,
                mDrawerLayout,
                R.string.drawer_open,
                R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    public void onClick(View v) {
        hideKeybord();
        switch (v.getId()) {
            case R.id.button_back:
                mDrawerLayout.openDrawer(drawerLayout);
                break;

            case R.id.button_add_another:
                validate();
                break;
            case R.id.button_save_address:
                validateAddress(1);
                break;
            case R.id.rl_search_map:
                intent = new Intent(PickUpDeliveryActivity.this, GoogleMapLocationCallActivity.class);
                startActivityForResult(intent, ADDRESS_REQUEST_CODE);
                break;
        }
    }

    private int isGooglePlayServicesAvailable() {
        //checking the google services(Internet Connection)
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        return status;

    }

    public void validate() {
        //will validate the data and add address string and will go to screenType or to find address
        currAddress.setZip(etZip.getText().toString());
        currAddress.setCity(etCity.getText().toString());
        currAddress.setStreet(etStreet.getText().toString());
        currAddress.setState(etState.getText().toString());
        currAddress.setSuite(etSuite.getText().toString());

        name = etName.getText().toString();
        mobile = etMobile.getText().toString();
        email = etEmail.getText().toString();

        Validations validations = new Validations();

        Boolean validAddress = validateAddress(0);

        Boolean validateName = validations.nameValidator(name);
        Boolean validateEmail = validations.emailValidator(email);

        // add other fields and validate
        if (validAddress) {

            if (mobile != null && mobile.length() >= 10 && validateName && validateEmail) {

                String agentAddress = currAddress.getSuite() + " " + currAddress.getStreet() + " " +
                        currAddress.getCity() + " " + currAddress.getState();

                pickUpObj.setDropOffDateTime(sdf.format(new Date()).toString());
                pickUpObj.setPickUpDateTime(sdf.format(new Date()).toString());

                if (screenView) {// means pickup
                    pickUpObj.setPickUpAddress(agentAddress);
                    pickUpObj.setPickUpName(name);
                    pickUpObj.setPickUpEmail(email);
                    pickUpObj.setPickUpPhoneNo(mobile);
                } else
                //means delivery
                {
                    pickUpObj.setDeliveryAddress(agentAddress);
                    pickUpObj.setDeliveryName(name);
                    pickUpObj.setDeliveryPhoneNo(mobile);
                    pickUpObj.setDeliveryEmail(email);
                }

            } else if (!validateName) {
                showToast("Invalid Name");
            } else if (!validateEmail) {
                showToast("Invalid Email");
            }
        } else
            showToast("In-Valid Address");

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == ADDRESS_REQUEST_CODE) && (resultCode == RESULT_OK)) ;
        String agentAddress = data.getStringExtra("address");
        int status = isGooglePlayServicesAvailable();
        if (agentAddress != null) {
            if (status == ConnectionResult.SUCCESS) {
                GeoCodingLocation convertLocationToLatLng = new GeoCodingLocation();
                convertLocationToLatLng.convertToAddressDetails(agentAddress,0, getApplicationContext(), new AddressHandler());
            } else
                showToast(MSG_NO_INTERNET_CONNECTION);
        } else
            showToast(NO_ADDRESS_RECIEVED);
    }


    //to find full address
    public class AddressHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            switch (msg.what) {
                case ADDRESS_FOUND_MESSAGE:
                    etCity.setText(bundle.getString(CITY));
                    etZip.setText(bundle.getString(ZIP));
                    etState.setText(bundle.getString(STATE));
                    etStreet.setText(bundle.getString(STREET));
                    etSuite.setText(bundle.getString(SUITE));
                    double[] latLong;
                    latLong = bundle.getDoubleArray(LAT_LONG_BUNDLE);
                    if (screenView) {
                        pickUpObj.setPickUpLatitude(latLong[0]);
                        pickUpObj.setPickUpLongitude(latLong[1]);
                    } else {

                        pickUpObj.setDeliveryLat(latLong[0]);
                        pickUpObj.setDeliveryLong(latLong[1]);
                    }
                    break;
                case ADDRESS_NOT_FOUND_MESSAGE:
                default:
                    showToast(MSG_ADDRESS_NOT_FOUND);
            }
        }
    }

    // to find only LatLong
    private class LatLngHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            double[] latLong = bundle.getDoubleArray(LAT_LONG_BUNDLE);
            int toSave = bundle.getInt(TO_SAVE, 0);
            switch (msg.what) {
                case ADDRESS_FOUND_MESSAGE:
                    currAddress.setLatitude(latLong[0]);
                    currAddress.setLongitude(latLong[1]);
                    if (toSave == 1) {
                        saveAddressApi();
                        return;
                    }

                    if (screenView) {
                        pickUpObj.setPickUpLatitude(currAddress.getLatitude());
                        pickUpObj.setPickUpLongitude(currAddress.getLongitude());
                    } else {

                        pickUpObj.setDeliveryLat(currAddress.getLatitude());
                        pickUpObj.setDeliveryLong(currAddress.getLongitude());
                    }
                    screenType(screenView);
                    break;
                case ADDRESS_NOT_FOUND_MESSAGE:
                default:
                    showToast(MSG_ADDRESS_NOT_FOUND);
            }
        }
    }

    public void screenType(Boolean screenView) {
        // true means pickup , false means delivery data

        if (screenView) {
            this.screenView = false;
            setActivityHeader(R.string.title_activity_pick_up_delivery);
            btNext.setText("Done");

            etCity.setText("");
            etState.setText("");
            etStreet.setText("");
            etSuite.setText("");
            etZip.setText("");
            etMobile.setText("");
            etEmail.setText("");
            etName.setText("");
            hideKeybord();
            currAddress = new AddressDetails();
        } else {
            pickUpTaskList.add(pickUpObj);

            startActivity(new Intent(PickUpDeliveryActivity.this, AddItemActivity.class));
            finish();
            hideProgressDialog();

        }
    }

    public Boolean validateAddress(int toSave) {
        currAddress.setZip(etZip.getText().toString());
        currAddress.setCity(etCity.getText().toString());
        currAddress.setStreet(etStreet.getText().toString());
        currAddress.setState(etState.getText().toString());
        currAddress.setSuite(etSuite.getText().toString());
        if (currAddress.getZip() != null && !currAddress.getZip().isEmpty()
                && currAddress.getCity() != null && !currAddress.getCity().isEmpty()
                && currAddress.getStreet() != null && !currAddress.getStreet().isEmpty()
                && currAddress.getSuite() != null && !currAddress.getSuite().isEmpty()
                && currAddress.getState() != null && !currAddress.getState().isEmpty()
                ) {
            String agentAddress = currAddress.getSuite() + " " + currAddress.getStreet() + " " +
                    currAddress.getCity() + " " + currAddress.getState();
            int status = isGooglePlayServicesAvailable();
            if (status != ConnectionResult.SUCCESS) {
                showToast(MSG_NO_INTERNET_CONNECTION);
            } else {
                GeoCodingLocation convertLocationToLatLng = new GeoCodingLocation();
                convertLocationToLatLng.convertToAddressDetails(agentAddress,toSave, getApplicationContext(), new LatLngHandler());
                return true;
            }
        }
        return false;
    }

    public void saveAddressApi() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(PickUpDeliveryActivity.this);
        String token = preferences.getString(ACCESS_TOKEN, "");

        new RestClient().getService().setAddress(BEARER + token, "str" + currAddress.getString(), currAddress.getZip(), currAddress.getCity(),
                currAddress.getStreet(), currAddress.getState(), currAddress.getSuite()
                , currAddress.getLatitude(), currAddress.getLongitude(), new Callback<SaveAddressResponse>() {
                    @Override
                    public void success(SaveAddressResponse addressDetails, Response response) {
                        showToast("Address Saved");
                        getFavAddress();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        hideProgressDialog();
                        showToast(error.getMessage());
                    }
                });

    }

    public void getFavAddress() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(PickUpDeliveryActivity.this);
        String token = preferences.getString(ACCESS_TOKEN, "");

        new RestClient().getService().getAddress(BEARER + token,
                new Callback<AddressRetrieve>() {
                    @Override
                    public void success(AddressRetrieve addressDetails, Response response) {
                        for (int i = 0; i < addressDetails.getData().length; i++) {
                            favAddress.add(addressDetails.getData()[i]);
                            drawerAdapter.notifyDataSetChanged();
                        }
                    }
                    @Override
                    public void failure(RetrofitError error) {
                        hideProgressDialog();
                        showToast(error.getMessage());
                    }
                });

    }
}
