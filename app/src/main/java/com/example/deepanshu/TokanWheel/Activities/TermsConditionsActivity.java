package com.example.deepanshu.TokanWheel.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.deepanshu.TokanWheel.R;

public class TermsConditionsActivity extends BaseActivity {
    TextView tvActivityStatus, tvTerms;
    ImageButton ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditions);
        init();
        setOnClick();
    }

    public void init() {
        tvActivityStatus = (TextView) findViewById(R.id.status_text);
        tvTerms = (TextView) findViewById(R.id.tv_terms);
        ivBack = (ImageButton) findViewById(R.id.button_back);
    }

    public void setOnClick() {
        tvActivityStatus.setText(R.string.title_activity_terms_conditions);
        tvTerms.setText(TERMS_AND_SERVICES);
        /* for getting the Terms and Conditions dynamically
        new RestClient().getService().getTerms(new Callback<TermsResponse>() {
            @Override
            public void success(TermsResponse termsResponse, Response response) {
                tvTerms.setText(termsResponse.toString());
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(TermsConditionsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });*/
        ivBack.setOnClickListener(TermsConditionsActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_back:
                super.onBackPressed();
                finish();
                break;
        }
    }
}
