package com.example.deepanshu.TokanWheel.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.example.deepanshu.TokanWheel.Utilities.Conventions;

/**
 * Created by Deepanshu on 12/18/2015.
 */
public abstract class BaseFragment extends Fragment implements Conventions, View.OnClickListener {
//extending the Fragments with some extra functionalities
    Toast toast;
    ProgressDialog dialog;
    Activity currActivity;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        currActivity = getActivity();
        toast = new Toast(currActivity);
    }

    public abstract void init();

    public abstract void setOnClick();

    public void hideKeybord() {
//to hide keyboard if shown
        InputMethodManager imm = (InputMethodManager) currActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive())
            imm.hideSoftInputFromWindow(currActivity.getCurrentFocus().getWindowToken(), 0);
    }

    public void showKeybord() {
//to show keyboard
        InputMethodManager imm = (InputMethodManager) currActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInputFromInputMethod(currActivity.getCurrentFocus().getWindowToken(), 0);
    }
    public void showProgressDialog(String header, String msg) {
        if (!dialog.isShowing()) {
            dialog.setTitle(header);
            dialog.setMessage(msg);
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    public void showProgressDialog(String msg) {
        if (!dialog.isShowing()) {
            dialog.setMessage(msg);
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    public void hideProgressDialog() {
        if (dialog.isShowing())
            dialog.dismiss();
    }

}
