package com.example.deepanshu.TokanWheel.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.deepanshu.TokanWheel.Entities.ApiUserResponse;
import com.example.deepanshu.TokanWheel.R;
import com.example.deepanshu.TokanWheel.Retrofit.RestClient;
import com.example.deepanshu.TokanWheel.Utilities.Validations;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SignInActivity extends BaseActivity {
    Button btSignIn, btForgot;
    TextView tvSignUp;
    EditText etEmail, etPassword;
    Validations validate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        init();
        setOnClick();
    }

    public void init() {
        tvSignUp = (TextView) findViewById(R.id.text_sign_up);
        btSignIn = (Button) findViewById(R.id.button_sign_in);
        btForgot = (Button) findViewById(R.id.button_forgot_password);
        etEmail = (EditText) findViewById(R.id.edit_email);
        etPassword = (EditText) findViewById(R.id.edit_password);
        validate = new Validations();
    }

    public void setOnClick() {
        setActivityHeader(R.string.title_activity_sign_in);
        setIvBackOnClick();
        tvSignUp.setText(Html.fromHtml(getString(R.string.not_register)));
        btForgot.setOnClickListener(SignInActivity.this);
        btSignIn.setOnClickListener(SignInActivity.this);
        tvSignUp.setOnClickListener(SignInActivity.this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_sign_up:
                startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
                finish();
                break;
            case R.id.button_sign_in:
                String uName = etEmail.getText().toString();
                String upwd = etPassword.getText().toString();
                validate(uName, upwd);
                break;
            case R.id.button_forgot_password:
                break;
        }
    }

    public void validate(String uName, String uPassword) {
        if (uName == null || uName.isEmpty() || uPassword.isEmpty() || uPassword == null) {
            showToast(NULL_VALUE);
        } else {
            hideKeybord();
            showProgressDialog(PROCESSING);
            new RestClient().getService().postLogin(uName, uPassword, DEVICE_TYPE, false, DEVICE_TOKEN,
                    new Callback<ApiUserResponse>() {
                        @Override
                        public void success(ApiUserResponse apiUserResponse, Response response) {
                            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SignInActivity.this);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString(ACCESS_TOKEN, apiUserResponse.getData().getToken().toString());
//                            editor.putString(USER_FIRST_NAME, apiUserResponse.getData().getAgent().getName().getFirstName().toString());
//                            editor.putString(USER_LAST_NAME, apiUserResponse.getData().getAgent().getName().getLastName().toString());
                            editor.commit();
                            startActivity(new Intent(SignInActivity.this, FragmentsMainActivity.class));
                            finish();
                            hideProgressDialog();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            hideProgressDialog();
                            showToast(error.getMessage());
                        }
                    });
        }

    }
}