package com.example.deepanshu.TokanWheel.Entities;

/**
 * Created by DEEPANSHU on 19-12-2015.
 */
public class AddressRetrieve {
    String statusCode, message;
    AddressDetails[] data;

    public AddressDetails[] getData() {
        return data;
    }

    public void setData(AddressDetails[] data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
