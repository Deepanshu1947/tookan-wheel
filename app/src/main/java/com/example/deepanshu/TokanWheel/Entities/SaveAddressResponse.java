package com.example.deepanshu.TokanWheel.Entities;

/**
 * Created by DEEPANSHU on 19-12-2015.
 */
public class SaveAddressResponse {
    int statusCode;
    String message;
    CustomData data;

    public CustomData getData() {
        return data;
    }

    public void setData(CustomData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    private class CustomData {
        int statusCode;
                String customMessage,type;

        public String getCustomMessage() {
            return customMessage;
        }

        public void setCustomMessage(String customMessage) {
            this.customMessage = customMessage;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(int statusCode) {
            this.statusCode = statusCode;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
