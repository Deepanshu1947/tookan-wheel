package com.example.deepanshu.TokanWheel.Activities;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.deepanshu.TokanWheel.R;
import com.example.deepanshu.TokanWheel.Utilities.Conventions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class ShowLocationOnMapActivity extends FragmentActivity implements OnMapReadyCallback, Conventions {
    GoogleMap googleMap;
    double[] latlng = new double[2];

    ImageButton ivBack;
    TextView tvActivityStatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_location_on_map);
        init();
    }

    public void init() {
        ivBack = (ImageButton) findViewById(R.id.button_back);
        tvActivityStatus = (TextView) findViewById(R.id.status_text);
        tvActivityStatus.setText(R.string.title_activity_google_map);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowLocationOnMapActivity.this.onBackPressed();
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        googleMap = mapFragment.getMap();

        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(this, MSG_NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();
        } else {
            double[] userLocation = getIntent().getDoubleArrayExtra("location");
            if (userLocation == null || userLocation[0] == 0.00 || userLocation[1] == 0.00) {
                Toast.makeText(ShowLocationOnMapActivity.this, MSG_ADDRESS_NOT_FOUND, Toast.LENGTH_SHORT).show();
                return;
            } else {
                latlng = userLocation;
                setLocation();
            }
        }
    }

    private void setLocation() {
        LatLng latLng = new LatLng(latlng[0], latlng[1]);
        googleMap.addMarker(new MarkerOptions().position(latLng).title(CUST_LOC).icon(BitmapDescriptorFactory.fromResource(R.mipmap.your_location_icon)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(10));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }


}
