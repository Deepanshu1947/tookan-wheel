package com.example.deepanshu.TokanWheel.Entities;

/**
 * Created by DEEPANSHU on 19-12-2015.
 */
public class InitialResponse {
    String statusCode, message;
    TaskData[] data;

    public TaskData[] getData() {
        return data;
    }

    public void setData(TaskData[] data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
