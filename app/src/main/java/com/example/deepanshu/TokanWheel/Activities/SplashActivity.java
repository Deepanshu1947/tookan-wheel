package com.example.deepanshu.TokanWheel.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;

import com.example.deepanshu.TokanWheel.R;
import com.google.android.gms.iid.InstanceID;


public class SplashActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
        setOnClick();
    }

    @Override
    public void init() {
        new RetrieveTokenTask().execute();

    }

    @Override
    public void setOnClick() {

    }

    @Override
    public void onClick(View v) {

    }

    private class RetrieveTokenTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(PROCESSING);
        }

        @Override
        protected String doInBackground(Void... params) {
            String scopes = "GCM";
            String token = null;
            try {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
                int returnVal = p1.waitFor();
                boolean reachable = (returnVal == 0);
                if (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting() && reachable) {
                    token = InstanceID.getInstance(SplashActivity.this).getToken(PROJECT_NUMBER, scopes);
                } else {
                    showToast(MSG_NO_INTERNET_CONNECTION);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return token;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s != null && !s.isEmpty()) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
                SharedPreferences.Editor editor = preferences.edit();
                // Edit the saved preferences
                editor.putString(DEVICE_TOKEN, s.toString());
                editor.commit();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
                        if (preferences == null || preferences.getString(ACCESS_TOKEN, "").toString().equals("")) {
                            SplashActivity.this.startActivity(new Intent(SplashActivity.this, SignInActivity.class));
                        } else {
                            SplashActivity.this.startActivity(new Intent(SplashActivity.this, FragmentsMainActivity.class));

                        }
                        SplashActivity.this.finish();
                        hideProgressDialog();
                    }
                }, SPLASH_SLEEP);
            } else {

                hideProgressDialog();
                new AlertDialog.Builder(SplashActivity.this).setTitle(MSG_NO_INTERNET_CONNECTION).
                        setMessage(RETRY).
                        setPositiveButton(YES, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new RetrieveTokenTask().execute();
                            }
                        }).
                        setNegativeButton(NO,
                                new android.content.DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        SplashActivity.this.finish();
                                    }
                                }).setCancelable(false)
                        .create().show();
            }
        }
    }

}
