
package com.example.deepanshu.TokanWheel.GoogleMapsApi;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.example.deepanshu.TokanWheel.Utilities.Conventions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GeoCodingLocation implements Conventions {

    public static void getLatLongFromAddress(final String locationAddress, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                double[] latLng = new double[2];
                int messageType = 0;
                try {
                    List addressList = geocoder.getFromLocationName(locationAddress, 1);
                    if (addressList != null && addressList.size() > 0) {
                        Address address = (Address) addressList.get(0);
                        latLng[0] = address.getLatitude();
                        latLng[1] = address.getLongitude();
                        messageType = ADDRESS_FOUND_MESSAGE;
                    } else {
                        latLng[0] = DEFAULT_LATITUDE;
                        latLng[1] = DEFAULT_LONGITUDE;
                        messageType = ADDRESS_NOT_FOUND_MESSAGE;
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Unable to connect to Geocoder", e);
                } finally {
                    Message message = Message.obtain();
                    message.what = messageType;
                    message.setTarget(handler);
                    Bundle bundle = new Bundle();
                    bundle.putDoubleArray(LAT_LONG_BUNDLE, latLng);
                    message.setData(bundle);
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }

    public static void convertToAddressDetails(final String locationAddress, final int toSave ,final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String zip = " ", city = " ", street = " ", state = " ", suite = " ";
                double[] latLong = new double[2];
                int messageType = 0;
                try {
                    List addressList = geocoder.getFromLocationName(locationAddress, 1);
                    if (addressList != null && addressList.size() > 0) {
                        Address address = (Address) addressList.get(0);
                        if (address.getPostalCode() != null)
                            zip = address.getPostalCode();
                        if (address.getLocality() != null)
                            city = address.getLocality();
                        if (address.getFeatureName() != null)
                            street = address.getFeatureName();
                        if (address.getAdminArea() != null)
                            state = address.getAdminArea();
                        if (address.getSubLocality() != null)
                            suite = address.getSubLocality();
                        messageType = ADDRESS_FOUND_MESSAGE;
                        latLong[0] = address.getLatitude();
                        latLong[1] = address.getLongitude();
                    } else {
                        messageType = ADDRESS_NOT_FOUND_MESSAGE;
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Unable to connect to Geocoder", e);
                } finally {
                    Message message = Message.obtain();
                    message.what = messageType;
                    message.setTarget(handler);
                    Bundle bundle = new Bundle();
                    bundle.putString(ZIP, zip);
                    bundle.putInt(TO_SAVE, toSave);
                    bundle.putString(CITY, city);
                    bundle.putString(STATE, state);
                    bundle.putString(STREET, street);
                    bundle.putString(SUITE, suite);
                    bundle.putDoubleArray(LAT_LONG_BUNDLE, latLong);
                    message.setData(bundle);
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }
}
