package com.example.deepanshu.TokanWheel.Retrofit;

import com.example.deepanshu.TokanWheel.Entities.AddressDetails;
import com.example.deepanshu.TokanWheel.Entities.AddressRetrieve;
import com.example.deepanshu.TokanWheel.Entities.InitialResponse;
import com.example.deepanshu.TokanWheel.Entities.SaveAddressResponse;
import com.example.deepanshu.TokanWheel.Entities.TermsResponse;
import com.example.deepanshu.TokanWheel.Entities.ApiUserResponse;

import org.json.JSONArray;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.PUT;

/**
 * Created by Deepanshu on 12/18/2015.
 */
public interface ApiService {


    //enter proper fields
    String GET_TERMS = "/api/agent/termsAndPolicy";
    String PUT_LOGOUT = "";


    String GET_ADDRESS = "/api/fav/getFavLocation";
    String SAVE_ADDRESS = "/api/fav/addFavLocation";
    String GET_DATA = "/api/Task/getTask";
    String POST_LOGIN = "/api/Customer/customerLogin";
    String POST_REGISTER = "/api/Customer/register";
    String POST_DATA = "/api/Task/createTask";


    @GET(GET_TERMS)
    void getTerms(Callback<TermsResponse> callback);

    @FormUrlEncoded
    @POST(POST_LOGIN)
    void postLogin(@Field("email") String email,
                   @Field("password") String password,
                   @Field("deviceType") String deviceType,
                   @Field("flushPreviousSessions") Boolean flushPreviousSessions,
                   @Field("deviceToken") String deviceToken, Callback<ApiUserResponse> callback);

    @FormUrlEncoded
    @POST(POST_DATA)
    void postAddress(@Header("authorization") String authorization,
                     @Field("taskDetails") JSONArray taskDetails,
                     Callback<ApiUserResponse> callback);

    @PUT(PUT_LOGOUT)
    void putLogout(@Header("authorization") String authorization, Callback<ApiUserResponse> callback);

    @POST(GET_ADDRESS)
    void getAddress(@Header("authorization") String authorization, Callback<AddressRetrieve> callback);

    @FormUrlEncoded
    @POST(SAVE_ADDRESS)
    void setAddress(@Header("authorization") String authorization,
                    @Field("String") String address,
                    @Field("zip") String zip,
                    @Field("city") String city,
                    @Field("street") String street,
                    @Field("state") String state,
                    @Field("suite") String suite,
                    @Field("latitude") double latitude,
                    @Field("longitude") double longitude,
                    Callback<SaveAddressResponse> callback);

    @POST(GET_DATA)
    void getData(
            @Header("authorization") String authorization, Callback<InitialResponse> callback);

    @FormUrlEncoded
    @POST(POST_REGISTER)
    void getRegister(
            @Field("email") String email,
//                     @Field("password") String password,
            @Field("phoneNo") String phoneNo,
            @Field("deviceType") String deviceType,
            @Field("deviceToken") String deviceToken,
            Callback<ApiUserResponse> callback);

}
